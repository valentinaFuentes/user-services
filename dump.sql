INSERT INTO public.aplicacion
(id_app, descripcion)
VALUES(2, 'SIAC');
INSERT INTO public.aplicacion
(id_app, descripcion)
VALUES(1, 'DIVAP');

INSERT INTO public.rol
(id_rol, nombre_rol, id_app_aplicacion)
VALUES(3, 'analyst', 1);
INSERT INTO public.rol
(id_rol, nombre_rol, id_app_aplicacion)
VALUES(5, 'evaluador', 2);
INSERT INTO public.rol
(id_rol, nombre_rol, id_app_aplicacion)
VALUES(11, 'ServicioSalud', 1);
INSERT INTO public.rol
(id_rol, nombre_rol, id_app_aplicacion)
VALUES(9, 'firmador', 2);
INSERT INTO public.rol
(id_rol, nombre_rol, id_app_aplicacion)
VALUES(7, 'visador', 2);
INSERT INTO public.rol
(id_rol, nombre_rol, id_app_aplicacion)
VALUES(2, 'admin', 1);
INSERT INTO public.rol
(id_rol, nombre_rol, id_app_aplicacion)
VALUES(4, 'operador', 2);
INSERT INTO public.rol
(id_rol, nombre_rol, id_app_aplicacion)
VALUES(10, 'Administrador', 1);
INSERT INTO public.rol
(id_rol, nombre_rol, id_app_aplicacion)
VALUES(8, 'visadorOIRS', 2);
INSERT INTO public.rol
(id_rol, nombre_rol, id_app_aplicacion)
VALUES(6, 'referenteTecnico', 2);
INSERT INTO public.rol
(id_rol, nombre_rol, id_app_aplicacion)
VALUES(1, 'Profesional', 1);

INSERT INTO public.usuario
(username, nombre, apellido, password)
VALUES('clemus', 'Carmen Rita', 'Lemus', 'WzrzlQ+Y/bFqXfIBPNMEPEa0T5F42dI1rgADDhtaNh8=');
INSERT INTO public.usuario
(username, nombre, apellido, password)
VALUES('operador@minsal.cl', 'Operador', 'Operador', '4evt40QK8m8X4AxEpTvI7IsbfrS19bJg0QRXmP5g1jo=');
INSERT INTO public.usuario
(username, nombre, apellido, password)
VALUES('maguilera', 'Macarena', 'Aguilera', 'LiSBz/6pwTKjPJT5LvLZgrWeT07tebS5E/O+zdBoUW0=');
INSERT INTO public.usuario
(username, nombre, apellido, password)
VALUES('visador.oirs@minsal.cl', 'Visador', 'Oirs', '4evt40QK8m8X4AxEpTvI7IsbfrS19bJg0QRXmP5g1jo=');
INSERT INTO public.usuario
(username, nombre, apellido, password)
VALUES('referente.tecnico@minsal.cl', 'Referente', 'Técnico', '4evt40QK8m8X4AxEpTvI7IsbfrS19bJg0QRXmP5g1jo=');
INSERT INTO public.usuario
(username, nombre, apellido, password)
VALUES('visador@minsal.cl', 'Visador', 'Visador', '4evt40QK8m8X4AxEpTvI7IsbfrS19bJg0QRXmP5g1jo=');
INSERT INTO public.usuario
(username, nombre, apellido, password)
VALUES('rtello', 'Rodolfo', 'Tello', 'G4/ntu1RxkZOEENXlokZy8E8uSbeJuX4teKnPLfmsDQ=');
INSERT INTO public.usuario
(username, nombre, apellido, password)
VALUES('bpmsAdmin', 'bpmsAdmin', 'bpmsAdmin', 'WNPBCApiucS3Ur2Ye9OLBOrpgPI5Mq0Hs5WGQCmJzOk=');
INSERT INTO public.usuario
(username, nombre, apellido, password)
VALUES('divap', 'divap', 'divap', '8RgprYjBTR1L7KRgW8fTPIvuwfKlipB2gO/tKjs0skM=');
INSERT INTO public.usuario
(username, nombre, apellido, password)
VALUES('evaluador.oirs@minsal.cl', 'Evaluador', 'Oirs', '4evt40QK8m8X4AxEpTvI7IsbfrS19bJg0QRXmP5g1jo=');
INSERT INTO public.usuario
(username, nombre, apellido, password)
VALUES('vrodriguez', 'Victor', 'Rodriguez', 'FQdPVGMFFvYj9ulYlHLPCj9WYs6ambZysX5nrAxNgnY=');
INSERT INTO public.usuario
(username, nombre, apellido, password)
VALUES('firmador@minsal.cl', 'Firmador', 'Firmador', '4evt40QK8m8X4AxEpTvI7IsbfrS19bJg0QRXmP5g1jo=');
INSERT INTO public.usuario
(username, nombre, apellido, password)
VALUES('mnavarro', 'Marcela', 'Navarro', 'xyHZJy8VZwrIYUiJ82AzE9Vi57S9nFRWXdA2tnaTFtg=');
INSERT INTO public.usuario
(username, nombre, apellido, password)
VALUES('aferran', 'Arminda', 'Ferran', 'NWjVqKgjUErZF0vou3sn1Eo+HeuNNrTNnpQper22DV0=');

INSERT INTO public.usuario_rol
(id_usuario_rol, id_rol_rol, username_usuario)
VALUES(3, 1, 'clemus');
INSERT INTO public.usuario_rol
(id_usuario_rol, id_rol_rol, username_usuario)
VALUES(14, 9, 'firmador@minsal.cl');
INSERT INTO public.usuario_rol
(id_usuario_rol, id_rol_rol, username_usuario)
VALUES(5, 1, 'maguilera');
INSERT INTO public.usuario_rol
(id_usuario_rol, id_rol_rol, username_usuario)
VALUES(10, 5, 'evaluador.oirs@minsal.cl');
INSERT INTO public.usuario_rol
(id_usuario_rol, id_rol_rol, username_usuario)
VALUES(9, 4, 'operador@minsal.cl');
INSERT INTO public.usuario_rol
(id_usuario_rol, id_rol_rol, username_usuario)
VALUES(12, 8, 'visador.oirs@minsal.cl');
INSERT INTO public.usuario_rol
(id_usuario_rol, id_rol_rol, username_usuario)
VALUES(7, 1, 'rtello');
INSERT INTO public.usuario_rol
(id_usuario_rol, id_rol_rol, username_usuario)
VALUES(2, 2, 'bpmsAdmin');
INSERT INTO public.usuario_rol
(id_usuario_rol, id_rol_rol, username_usuario)
VALUES(4, 3, 'divap');
INSERT INTO public.usuario_rol
(id_usuario_rol, id_rol_rol, username_usuario)
VALUES(11, 6, 'referente.tecnico@minsal.cl');
INSERT INTO public.usuario_rol
(id_usuario_rol, id_rol_rol, username_usuario)
VALUES(8, 1, 'vrodriguez');
INSERT INTO public.usuario_rol
(id_usuario_rol, id_rol_rol, username_usuario)
VALUES(13, 7, 'visador@minsal.cl');
INSERT INTO public.usuario_rol
(id_usuario_rol, id_rol_rol, username_usuario)
VALUES(6, 1, 'mnavarro');
INSERT INTO public.usuario_rol
(id_usuario_rol, id_rol_rol, username_usuario)
VALUES(1, 1, 'aferran');

SELECT setval('sec_usuario_rol', (SELECT COUNT(*) FROM usuario_rol));

