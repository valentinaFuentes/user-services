package cl.minsal.users.model;

import java.io.Serializable;

import javax.persistence.*;


/**
 * The persistent class for the usuario_rol database table.
 * 
 */
@Entity
@Table(name = "usuario_rol")
@NamedQuery(name = "UsuarioRol.findAll", query = "SELECT u FROM UsuarioRol u")
public class UsuarioRol implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id_usuario_rol")
	private Long idUsuarioRol;

	// bi-directional many-to-one association to Rol
	@ManyToOne
	@JoinColumn(name = "id_rol_rol")
	private Rol rol;

	// bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name = "username_usuario")
	private Usuario usuario;

	public UsuarioRol() {
	}

	public Long getIdUsuarioRol() {
		return this.idUsuarioRol;
	}

	public void setIdUsuarioRol(Long idUsuarioRol) {
		this.idUsuarioRol = idUsuarioRol;
	}

	public Rol getRol() {
		return this.rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	public Usuario getUsuario() {
		return this.usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public String toString() {
		return "UsuarioRol [idUsuarioRol=" + idUsuarioRol + ", rol=" + rol
				+ ", usuario=" + usuario + "]";
	}
	
	
	
}