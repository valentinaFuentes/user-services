package cl.minsal.users.model;

import java.io.Serializable;

import javax.persistence.*;

import org.codehaus.jackson.annotate.JsonIgnore;

import java.util.List;


/**
 * The persistent class for the aplicacion database table.
 * 
 */
@Entity
@NamedQuery(name="Aplicacion.findAll", query="SELECT a FROM Aplicacion a")
public class Aplicacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id_app")
	private Integer idApp;

	private String descripcion;

	//bi-directional many-to-one association to Rol
	@OneToMany(mappedBy="aplicacion")
	@JsonIgnore
	private List<Rol> rols;

	public Aplicacion() {
	}

	public Integer getIdApp() {
		return this.idApp;
	}

	public void setIdApp(Integer idApp) {
		this.idApp = idApp;
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public List<Rol> getRols() {
		return this.rols;
	}

	public void setRols(List<Rol> rols) {
		this.rols = rols;
	}

	public Rol addRol(Rol rol) {
		getRols().add(rol);
		rol.setAplicacion(this);

		return rol;
	}

	public Rol removeRol(Rol rol) {
		getRols().remove(rol);
		rol.setAplicacion(null);

		return rol;
	}

}