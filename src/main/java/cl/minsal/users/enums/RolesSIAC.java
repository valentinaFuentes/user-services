package cl.minsal.users.enums;

/**
 * @author psep
 *
 */
public enum RolesSIAC {

	BPMS_ADMIN(14), ANALYST(15), KIE_SERVER(16), REST_CLIENT(17);

	private int id;

	private RolesSIAC(int id) {
		this.id = id;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

}
