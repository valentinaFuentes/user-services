package cl.minsal.users.bean;

import java.util.HashSet;
import java.util.Set;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author psep
 *
 */
@ApplicationPath(value = "/*")
public class RESTApplication extends Application {
	
	private Set<Object> singletons = new HashSet<Object>();
	private Set<Class<?>> classes = new HashSet<Class<?>>();
	
	/**
	 * Constructor de la clase.
	 */
	public RESTApplication() {
		classes.add(UsuarioBean.class);
		classes.add(AplicacionBean.class);
		classes.add(RolBean.class);
		classes.add(UsuarioRolBean.class);
	}
	
	@Override
	public Set<Object> getSingletons() {
		return singletons;
	}

	@Override
	public Set<Class<?>> getClasses() {
		return classes;
	}

}
