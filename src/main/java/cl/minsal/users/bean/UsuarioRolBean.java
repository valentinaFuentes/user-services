package cl.minsal.users.bean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.jboss.logging.Logger;

import cl.minsal.users.ejb.UsuarioRolEJB;
import cl.minsal.users.enums.RolesSIAC;
import cl.minsal.users.model.Rol;
import cl.minsal.users.model.UsuarioRol;

@Path("/UsuarioRol")
@RequestScoped
public class UsuarioRolBean {

	private static final Logger logger = Logger.getLogger(UsuarioRolBean.class);

	@Inject
	private UsuarioRolEJB usuarioRolEJB;

	@GET
	@Path("/retrieve/list/all")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<UsuarioRol> list() {
		return this.usuarioRolEJB.list();
	}

	@GET
	@Path("/retrieve/id/{userId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public UsuarioRol findById(@PathParam("userId") String userId) {
		return this.usuarioRolEJB.findById(Long.valueOf(userId));
	}

	@POST
	@Path("/create")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Boolean create(UsuarioRol usuarioRol) {
		List<UsuarioRol> roles = new ArrayList<UsuarioRol>();

		Rol rolAnalyst = new Rol();
		rolAnalyst.setIdRol(RolesSIAC.ANALYST.getId());
		UsuarioRol usuarioAnalyst = new UsuarioRol();
		usuarioAnalyst.setUsuario(usuarioRol.getUsuario());
		usuarioAnalyst.setRol(rolAnalyst);
		roles.add(usuarioAnalyst);

		Rol rolKie = new Rol();
		rolKie.setIdRol(RolesSIAC.KIE_SERVER.getId());
		UsuarioRol usuarioKie = new UsuarioRol();
		usuarioKie.setUsuario(usuarioRol.getUsuario());
		usuarioKie.setRol(rolKie);
		roles.add(usuarioKie);

		Rol rolRest = new Rol();
		rolRest.setIdRol(RolesSIAC.REST_CLIENT.getId());
		UsuarioRol usuarioRest = new UsuarioRol();
		usuarioRest.setUsuario(usuarioRol.getUsuario());
		usuarioRest.setRol(rolRest);
		roles.add(usuarioRest);

		return this.createRoles(roles);
	}

	@POST
	@Path("/crearRoles")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Boolean crearRoles(UsuarioRol roles) {

		logger.info("usuario crear roles 83" + roles.getUsuario().getUsername());

		List<UsuarioRol> listroles = new ArrayList<UsuarioRol>();

		Rol rol = new Rol();
		rol.setIdRol(roles.getRol().getIdRol());
		UsuarioRol usuario = new UsuarioRol();
		usuario.setUsuario(roles.getUsuario());
		usuario.setRol(rol);
		listroles.add(usuario);

		return this.createRoles(listroles);
	}

	@POST
	@Path("/eliminarRoles")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Boolean eliminarRoles(String idUsuarioRol) {

		logger.info("entre a eliminar rol");
		
		Iterator<UsuarioRol> uR = this.usuarioRolEJB.list().iterator();

		while (uR.hasNext()) {

			UsuarioRol usuarioRolOld = uR.next();

			logger.info("usuarioRolOld username"+usuarioRolOld.getUsuario().getUsername());
			
			String usuarioSinCom=idUsuarioRol.substring(1,idUsuarioRol.length()-1);
			logger.info("usuarioRol id"+usuarioSinCom);
			
			if(usuarioRolOld.getUsuario().getUsername().equals(usuarioSinCom)){

				if (usuarioRolOld.getRol().getIdRol() != 15) {
					if (usuarioRolOld.getRol().getIdRol() != 16) {
						if (usuarioRolOld.getRol().getIdRol() != 17) {
							
							UsuarioRol u= usuarioRolEJB.findById(usuarioRolOld.getIdUsuarioRol());
							
							
							logger.info("usuario rol a eliminar" +u.getRol().getIdRol());
							
							delete(u);								
						}
					}
				}		
				
			}
			
		}
		
		return true;	

	}

	@POST
	@Path("/actualizarUsuarioRol")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Boolean actualizarUsuarioRol(UsuarioRol usuarioRol) {

		try {

			
				logger.info("usuarioRol a crear"
						+ usuarioRol.getRol().getIdRol());

				this.crearRoles(usuarioRol);
			
			

			return true;

		} catch (Exception e) {
			logger.error(e, e);
		}
		return null;

	}

	/**
	 * @param roles
	 * @return
	 */
	private Boolean createRoles(List<UsuarioRol> roles) {
		Iterator<UsuarioRol> it = roles.iterator();

		while (it.hasNext()) {
			UsuarioRol usuario = (UsuarioRol) it.next();

			if (!this.usuarioRolEJB.create(usuario)) {
				return false;
			}
		}

		return true;
	}

	@POST
	@Path("/update")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Boolean update(UsuarioRol usuarioRol) {
		return this.usuarioRolEJB.update(usuarioRol);
	}

	@POST
	@Path("/delete")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Boolean delete(UsuarioRol usuarioRol) {
		return this.usuarioRolEJB.delete(usuarioRol);
	}
}
