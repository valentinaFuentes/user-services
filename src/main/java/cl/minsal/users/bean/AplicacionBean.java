package cl.minsal.users.bean;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import cl.minsal.users.ejb.AplicacionEJB;
import cl.minsal.users.model.Aplicacion;


@Path("/Aplicacion")
@RequestScoped
public class AplicacionBean {
	
	
	/**
	 * 
	 */
	@Inject
	private AplicacionEJB aplicacionEJB;

	/**
	 * @return
	 */
	@GET
	@Path("/retrieve/list/all")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Aplicacion> list() {
		return this.aplicacionEJB.list();
	}
	
	@GET
	@Path("/retrieve/id/{userId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Aplicacion findById(@PathParam("userId") String userId) {
		return this.aplicacionEJB.findById(Integer.parseInt(userId));
	}
	
	@POST
	@Path("/create")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Boolean create(Aplicacion aplicacion) {
		return this.aplicacionEJB.create(aplicacion);
	}
	
	@POST
	@Path("/update")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Boolean update(Aplicacion aplicacion) {
		return this.aplicacionEJB.update(aplicacion);
	}
	
	@POST
	@Path("/delete")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Boolean delete(Aplicacion aplicacion) {
		return this.aplicacionEJB.delete(aplicacion);
	}
}
