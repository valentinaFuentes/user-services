package cl.minsal.users.bean;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import cl.minsal.users.ejb.RolEJB;
import cl.minsal.users.model.Rol;

@Path("/Rol")
@RequestScoped
public class RolBean {
	
	/**
	 * 
	 */
	@Inject
	private RolEJB rolEJB;

	/**
	 * @return
	 */
	@GET
	@Path("/retrieve/list/all")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Rol> list() {
		return this.rolEJB.list();
	}
	
	@GET
	@Path("/retrieve/id/{userId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Rol findById(@PathParam("userId") String userId) {
		return this.rolEJB.findById(Integer.parseInt(userId));
	}
	
	@POST
	@Path("/create")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Boolean create(Rol rol) {
		return this.rolEJB.create(rol);
	}
	
	@POST
	@Path("/update")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Boolean update(Rol rol) {
		return this.rolEJB.update(rol);
	}
	
	@POST
	@Path("/delete")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Boolean delete(Rol rol) {
		return this.rolEJB.delete(rol);
	}

}
