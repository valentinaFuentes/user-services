package cl.minsal.users.bean;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import cl.minsal.users.ejb.UsuarioEJB;
import cl.minsal.users.model.Usuario;

/**
 * @author psep
 *
 */
@Path("/Usuario")
@RequestScoped
public class UsuarioBean {

	/**
	 * 
	 */
	@Inject
	private UsuarioEJB usuarioEJB;

	/**
	 * @return
	 */
	@GET
	@Path("/retrieve/list/all")
	@Produces({ MediaType.APPLICATION_JSON })
	public List<Usuario> list() {
		return this.usuarioEJB.list();
	}
	
	@GET
	@Path("/retrieve/id/{userId}")
	@Produces({ MediaType.APPLICATION_JSON })
	public Usuario findById(@PathParam("userId") String userId) {
		return this.usuarioEJB.findById(userId);
	}
	
	@POST
	@Path("/create")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Boolean create(Usuario usuario) {
		return this.usuarioEJB.create(usuario);
	}
	
	@POST
	@Path("/update")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Boolean update(Usuario usuario) {
		return this.usuarioEJB.update(usuario);
	}
	
	@POST
	@Path("/delete")
	@Consumes({ MediaType.APPLICATION_JSON })
	public Boolean delete(Usuario usuario) {
		return this.usuarioEJB.delete(usuario);
	}

}
