package cl.minsal.users.ejb;

import java.util.List;
import javax.ejb.Stateless;
import org.jboss.logging.Logger;
import cl.minsal.users.model.Usuario;

/**
 * @author psep
 *
 */
@Stateless
public class UsuarioEJB extends AbstractEJB {

	private static final Logger logger = Logger.getLogger(UsuarioEJB.class);

	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Usuario> list() {
		try {
			return (List<Usuario>) super.list("Usuario.findAll");

		} catch (Exception e) {
			logger.error(e, e);
		}

		return null;
	}

	/**
	 * @param id
	 * @return
	 */
	public Usuario findById(String id) {
		try {
			return super.findById(id, Usuario.class);

		} catch (Exception e) {
			logger.error(e, e);
		}

		return null;
	}

}
