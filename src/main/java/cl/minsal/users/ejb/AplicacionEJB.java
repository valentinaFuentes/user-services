package cl.minsal.users.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.jboss.logging.Logger;

import cl.minsal.users.model.Aplicacion;

@Stateless
public class AplicacionEJB extends AbstractEJB{

	private static final Logger logger = Logger.getLogger(AplicacionEJB.class);

	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Aplicacion> list() {
		try {
			return (List<Aplicacion>) super.list("Aplicacion.findAll");

		} catch (Exception e) {
			logger.error(e, e);
		}

		return null;
	}

	/**
	 * @param id
	 * @return
	 */
	public Aplicacion findById(int id) {
		try {
			return super.findById(id, Aplicacion.class);

		} catch (Exception e) {
			logger.error(e, e);
		}

		return null;
	}
}
