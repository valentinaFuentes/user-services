package cl.minsal.users.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.jboss.logging.Logger;

import cl.minsal.users.model.Usuario;
import cl.minsal.users.model.UsuarioRol;

@Stateless
public class UsuarioRolEJB extends AbstractEJB {

	private static final Logger logger = Logger.getLogger(UsuarioRolEJB.class);

	@SuppressWarnings("unchecked")
	public List<UsuarioRol> list() {
		try {

			return (List<UsuarioRol>) super.list("UsuarioRol.findAll");

		} catch (Exception e) {
			logger.error(e, e);
		}
		return null;
	}

	/**
	 * @param id
	 * @return
	 */
	public UsuarioRol findById(Long id) {
		try {
			return super.findById(id, UsuarioRol.class);

		} catch (Exception e) {
			logger.error(e, e);
		}

		return null;
	}
	
	
	
	
}
