package cl.minsal.users.ejb;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.jboss.logging.Logger;

/**
 * @author psep
 *
 */
public abstract class AbstractEJB {
	
	private static final Logger logger = Logger.getLogger(AbstractEJB.class);

	/**
	 * Atributo que obtiene la persistencia</br>
	 * mediante contexto.
	 */
	@PersistenceContext(name = "")
	private EntityManager em;
	
	/**
	 * @param jql
	 * @return
	 */
	protected List<?> list(String jql) {
		try {
			Query query = this.em.createNamedQuery(jql);
			
			return query.getResultList();

		} catch (Exception e) {
			logger.error(e, e);
		}

		return null;
	}
	
	/**
	 * @param id
	 * @return
	 */
	protected <T> T findById(Object id, Class<T> className) {
		try {
			return this.em.find(className, id);
			
		} catch (Exception e) {
			logger.error(e, e);
		}
		
		return null;
	}
	
	/**
	 * @param object
	 * @return
	 */
	public Boolean create(Object object) {
		try {
			this.em.persist(object);
			
			return true;
			
		} catch (Exception e) {
			logger.error(e, e);
		}
		
		return false;
	}
	
	/**
	 * @param object
	 * @return
	 */
	public Boolean update(Object object) {
		try {
			this.em.merge(object);
			return true;
			
		} catch (Exception e) {
			logger.error(e, e);
		}
		
		return false;
	}
	
	/**
	 * @param object
	 * @return
	 */
	public Boolean delete(Object object) {
		try {
			
			this.em.remove(this.em.merge(object));
			this.em.flush();
			
			return true;
			
		} catch (Exception e) {
			logger.error(e, e);
		}
		
		return false;
	}

	
	/**
	 * @param jql
	 * @param id
	 * @param param
	 * @return
	 */
	protected List<?> list(String jql, String id, Object param) {
		try {
			Query query = this.em.createNamedQuery(jql);
			query.setParameter(id, param);
			
			return query.getResultList();

		} catch (Exception e) {
			logger.error(e, e);
		}

		return null;
	}
}
