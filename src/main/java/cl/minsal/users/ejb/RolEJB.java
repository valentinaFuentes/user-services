package cl.minsal.users.ejb;

import java.util.List;

import javax.ejb.Stateless;

import org.jboss.logging.Logger;

import cl.minsal.users.model.Rol;

@Stateless
public class RolEJB extends AbstractEJB{

	private static final Logger logger = Logger.getLogger(RolEJB.class);

	/**
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Rol> list() {
		try {
			return (List<Rol>) super.list("Rol.findAll");

		} catch (Exception e) {
			logger.error(e, e);
		}

		return null;
	}

	/**
	 * @param id
	 * @return
	 */
	public Rol findById(int id) {
		try {
			return super.findById(id, Rol.class);

		} catch (Exception e) {
			logger.error(e, e);
		}

		return null;
	}
}
